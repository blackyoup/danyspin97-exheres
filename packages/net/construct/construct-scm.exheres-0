# Coyright 2018-2021 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

ROCKSDB_VERSION="6.14.6"
SCM_rocksdb_REPOSITORY="https://github.com/facebook/rocksdb"
SCM_rocksdb_ACTUAL_REVISION="ed4316166f67ec892603014634840d29f460f611"
SCM_SECONDARY_REPOSITORIES="rocksdb"
SCM_EXTERNAL_REFS=" deps/boost: deps/gecko-dev: deps/pbc:"
SCM_EXTERNAL_REFS+=" deps/rocksdb:rocksdb doc: "

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="The first matrix server written in C++"
HOMEPAGE="https://github.com/matrix-${PN}/${PN}"
SCM_REPOSITORY="${HOMEPAGE}"

require scm-git

LICENCES="custom"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = [ exactly-one ] ]]
"

DEPENDENCIES="
    build+run:
        app-arch/bzip2
        app-arch/lz4
        app-arch/snappy
        app-arch/xz
        app-arch/zstd
        dev-db/rocksdb[=${ROCKSDB_VERSION}]
        dev-libs/boost[>=1.71]
        dev-libs/icu:=
        dev-libs/libsodium
        media-gfx/GraphicsMagick
        sys-apps/file
        sys-libs/zlib
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-logdir=/var/log/construct
    --datadir=/usr/share
)

export AT_M4DIR=( tools/m4 )

src_prepare() {
    edo sed -e '/text-unlikely-segment/d' -i "${WORK}"/configure.ac

    autotools_src_prepare
}

src_install() {
    default

    keepdir /var/log/${PN}
}

